import java.util.Scanner;

public class Challange1 {
    public static void main(String[] args) {
        loop:
        while (true) {
            System.out.print("-------------------------------------\nKalkulator penghitung Luas dan Volume\n-------------------------------------\nMenu\n1. Hitung Luas Bidang\n2. Hitung Volume\n0. Tutup Aplikasi\nPilihan Anda : ");
            Scanner scanner = new Scanner(System.in);
            int pilihan = scanner.nextInt();

            switch (pilihan) {
                case 1 -> {
                    System.out.print("-------------------------------------\nPilih bidang yang akan dihitung\n-------------------------------------\n1. Persegi\n2. Lingkaran\n3. Segitiga\n4. Persegi Panjang\n0. Kembali ke menu sebelumnya\nPilihan Anda : ");
                    int pilihan1 = scanner.nextInt();
                    if (pilihan1 == 1) {
                        System.out.print("Masukkan sisi persegi : ");
                        int s = scanner.nextInt();
                        luasPersegi(s);
                    } else if (pilihan1 == 2) {
                        System.out.print("Masukkan jari-jari lingkaran : ");
                        int j = scanner.nextInt();
                        luasLingkaran(j);
                    } else if (pilihan1 == 3) {
                        System.out.print("Masukkan alas segitiga : ");
                        int a = scanner.nextInt();
                        System.out.print("Masukkan tinggi segitiga : ");
                        int t = scanner.nextInt();
                        luasSegitiga(a, t);
                    } else if (pilihan1 == 4) {
                        System.out.print("Masukkan panjang persegi panjang : ");
                        int p = scanner.nextInt();
                        System.out.print("Masukkan lebar persegi panjang : ");
                        int l = scanner.nextInt();
                        luasPersegiPanjang(p, l);
                    } else if (pilihan1 == 0) {
                        continue;
                    } else {
                        System.out.println("Pilihan salah");
                    }
                    pressAnyKeyToContinue();
                }
                case 2 -> {
                    System.out.print("-------------------------------------\nPilih bidang yang akan dihitung\n-------------------------------------\n1. Kubus\n2. Balok\n3. Tabung\n0. Kembali ke menu sebelumnya\nPilihan Anda : ");
                    int pilihan2 = scanner.nextInt();
                    if (pilihan2 == 1) {
                        System.out.print("Masukkan sisi kubus : ");
                        int s = scanner.nextInt();
                        volumeKubus(s);
                    } else if (pilihan2 == 2) {
                        System.out.print("Masukkan panjang balok : ");
                        int p = scanner.nextInt();
                        System.out.print("Masukkan lebar balok : ");
                        int l = scanner.nextInt();
                        System.out.print("Masukkan tinggi balok : ");
                        int t = scanner.nextInt();
                        volumeBalok(p, l, t);

                    } else if (pilihan2 == 3) {
                        System.out.print("Masukkan tinggi tabung : ");
                        int tinggi = scanner.nextInt();
                        System.out.print("Masukkan jari-jari tabung : ");
                        int jari = scanner.nextInt();
                        volumeTabung(tinggi, jari);
                    } else if (pilihan2 == 0) {
                        continue;
                    } else {
                        System.out.println("Pilihan salah");
                    }
                    pressAnyKeyToContinue();
                }
                case 0 -> {
                    break loop;
                }
                default -> System.out.println("Pilihan salah");
            }

        }
        System.out.println("---------- Program Selesai ----------");
    }

    private static void luasPersegi(int sisi) {
        System.out.println("\nProcessing...\n");
        System.out.println("Luas dari persegi adalah " + sisi * sisi);
    }

    private static void luasLingkaran(int jari) {
        System.out.println("\nProcessing...\n");
        System.out.println("Luas dari lingkaran adalah " + (3.14 * Math.pow(jari, 2)));

    }

    private static void luasSegitiga(int alas, int tinggi) {
        System.out.println("\nProcessing...\n");
        System.out.println("Luas dari segitiga adalah " + (0.5 * alas * tinggi));
    }

    private static void luasPersegiPanjang(int panjang, int lebar) {
        System.out.println("\nProcessing...\n");
        System.out.println("Luas dari persegi panjang adalah " + (panjang * lebar));
    }

    private static void volumeKubus(int sisi) {
        System.out.println("\nProcessing...\n");
        System.out.println("Volume dari kubus adalah " + Math.pow(sisi, 3));
    }

    private static void volumeBalok(int panjang, int lebar, int tinggi) {
        System.out.println("\nProcessing...\n");
        System.out.println("Volume dari balok adalah " + panjang * lebar * tinggi);
    }

    private static void volumeTabung(int tinggi, int jari) {
        System.out.println("\nProcessing...\n");
        System.out.println("Volume dari tabung adalah " + (3.14 * tinggi * (Math.pow(jari, 2))));
    }

    private static void pressAnyKeyToContinue() {
        System.out.println("------------------------------\nTekan enter untuk kembali ke menu utama");
        try {
            System.in.read();
        } catch (Exception ignored) {
        }
    }
}
